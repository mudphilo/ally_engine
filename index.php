<?php

use Phalcon\Mvc\Micro;
use Phalcon\Loader;
use Phalcon\Di\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;
use Phalcon\Http\Response;
use Phalcon\Mvc\Micro\Collection as MicroCollection;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\File as FileAdapter;
use Phalcon\Http\Request;

	date_default_timezone_set('Africa/Nairobi');

error_reporting(E_ALL);

define('APP_PATH',realpath(''));

/**
 * Read the configuration
 */
$config = include APP_PATH . "/app/config/config.php";

/**
 * Read auto-loader
 */
include APP_PATH . "/app/config/loader.php";

/**
 * Read services
 */
include APP_PATH . "/app/config/services.php";

/**
 * Read composer libraries
 */
include APP_PATH . "/vendor/autoload.php";

//create and bind the DI to the application 
$app = new Micro($di);

$user_route = new MicroCollection();
$user_route->setPrefix('/v1/user/');
$user_route->setHandler('UserController',true);
$user_route->post('get','get');
$user_route->post('create','create');

$app->mount($user_route);

try
{
    // Handle the request
    $response = $app->handle();
}
catch (\Exception $e)
{
    echo date("Y-m-d H:i:s") . ' ' . $e->getMessage() . " (ง'̀-'́)ง I wanna go home! Get me out of here please!! ༼ つ ಥ_ಥ ༽つ Am lost this page is not available ".$e->getTraceAsString();
}