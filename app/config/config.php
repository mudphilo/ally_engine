<?php

defined('APP_PATH') || define('APP_PATH',realpath('.'));
defined('INFO_FILE') || define('INFO_FILE','info.log');
defined('ERROR_FILE') || define('ERROR_FILE','error.log');
defined('AUDIT_FILE') || define('AUDIT_FILE','audit.log');

$connection = [];
$logPath    = [];

$hostname = gethostname();

if ($hostname == 'legitimate-technology.com')
{

    $connection = [
        'adapter'  => 'Mysql',
        'host'     => 'localhost',
        'username' => 'root',
        'password' => 'legitimate',
        'dbname'   => 'ally_engine',
        'charset'  => 'utf8',
    ];

    $logs = [
        'directory' => "/var/www/logs/ally_engine/",
        'info'      => "info.log",
        'error'     => "error.log"
    ];

	$fileUpload = [
		'directory'=>'/var/www/logs/pledge/post/',
		'fileExtension'=>'csv',
		'size' => '2'
	];

    $logDir     = '/var/www/logs/pledge/';
}
else
{

    $connection = [
        'adapter'  => 'Mysql',
        'host'     => 'localhost',
        'username' => 'root',
        'password' => 'kingsize',
        'dbname'   => 'pledge_ms',
        'charset'  => 'utf8',
    ];

    $logs = [
        'directory' => "/Library/Logs/pledge/",
        'info'      => "info.log",
        'error'     => "error.log"
    ];

    $fileUpload = [
        'directory'     => '/Library/Logs/pledge/',
        'fileExtension' => 'png,jpeg,jpg,gif',
        'size'          => '2'
    ];

    $rootUri = "http://localhost/imovie_api_v1/";
    $logDir  = '/Library/Logs/pledge';
}

defined('LOG_PATH') || define('LOG_PATH',$logDir);

$firebase = [
    'key' => 'AAAAhiLToWg:APA91bGxb_I05XjUnesRujDnxu8_rt9mHVsP0WqazkTXruag9knmZ1Gl45v87M3S27qhDQia7K_kuPeHq0jt_Fgq-gDTQWEbAj7bpYh8pDsrC8pIlO3GOP6jKip0mTnkikjwwc_QJPpM',
    'id'  => '576109912424'];

$AFRICASTKNG = [
    'key'      => 'AAAAhiLToWg:',
    'username' => 'mudphilo'];

$bonus = [
    'referral' => ['referrer'=>20,'newUser'=>20,'firstDownload'=>20]
];

	$sms = [
		'senderId'=>'Uwazii',
		'username'=>'mudphilo',
		'password'=>'mudphilo',
		'url'=>'http://107.20.199.106/restapi/sms/1/text/single',
	];

return new \Phalcon\Config([
    'database'    => $connection,
    'firebase'    => $firebase,
    'AFRICASTKNG' => $AFRICASTKNG,
    'bonus'       => $bonus,
	'sms'       => $sms,
	'fileUpload' =>$fileUpload,
	'mongodb' => [
		'host'     => 'localhost',
		'port'     => 27017,
		'database' => 'ally_chat'
	],
	'application' => [
        'appID'          => 'com.legitimate.imovieKE',
        'controllersDir' => APP_PATH . '/app/controllers/',
        'modelsDir'      => APP_PATH . '/app/models/',
        'viewsDir'       => APP_PATH . '/app/views',
        'utilsDir'       => APP_PATH . '/app/utils/',
        'pluginsDir'     => APP_PATH . '/app/plugins/',
        'libraryDir'     => APP_PATH . '/app/library/',
        'cacheDir'       => APP_PATH . '/app/cache/',
        'vendorDir'      => APP_PATH . '/vendor/',
        'baseUri'        => preg_replace('/public([\/\\\\])index.php$/','',$_SERVER["PHP_SELF"]),
    ],
    'logPath'     => $logPath
]);
