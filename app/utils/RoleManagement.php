<?php
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Query\Builder as Builder;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\File as FileAdapter;
use Phalcon\Http\Request;

class RoleManagement
{
	public function checkPermission(){ //{userId, module, action,clientId}
		$request = new Request();
		$json = $request->getJsonRawBody();
		$userId = $json->userId;
		$clientId = $json->clientId;
		$module = $json->module;
		$action = $json->action;
		//get user  from user client table check client 
		$clientUser = ClientUser::findFirst('userId=$userId AND clientId=$clientId');
		//get user roles
		$query = "SELECT rp.status FROM RolePermission rp JOIN Role r on rp.roleId=r.roleId INNER JOIN permission p on rp.permissionId=p.permissionId WHERE rp.roleId=$clientUser->roleId AND p.action=$action AND p.module=$module";
        $status = RolePermission::execute($query);

        if($status == 1){
        	return true;
        }
        elseif($status == 0)
        {
        	return false;
        }
        else{
        	return false;
        }
    	
	}

	public function checkUserExistence($userId){
        $user = new UserController(); 
		 $user = $user->getUser($userId,1);

        if($user){
            //return $res->unProcessable("User doesn't exist",$userId);
            return true;
        }
        return false;
	}

}