<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Query\Builder as Builder;
use \Firebase\JWT\JWT;

class FirebaseMessageController extends ControllerBase
{

    public function indexAction()
    {
        
    }

    public function sendPush($devices,$type,$data)
    {
        $key       = "to";
        $fcm_array = array();
        if (is_array($devices))
        {
            $token = array();
            $key   = "registration_ids";

            foreach ($devices as $device)
            {
                $token[]        = $device->device_token;
                $fcm            = new FirebaseMessage();
                $fcm->device_id = $device->device_id;
                $fcm->message   = json_encode($data);
                $fcm->status    = 0;
                $fcm->type      = $type;
                $fcm->created      = date("Y-m-d H:i:s");
                if ($fcm->save() === false)
                {
                    $error = $fcm->getMessages();
                    $this->log('error',' line '.__FILE__.'.'.__LINE__.' creating FCM ' . json_encode($error));
                    return;
                }
                else
                {
                    $fcm_array[] = $fcm;
                }
            }
        }
        else
        {
            $token          = $devices->device_token;
            $fcm            = new FirebaseMessage();
            $fcm->device_id = $devices->device_id;
            $fcm->message   = json_encode($data);
            $fcm->status    = 0;
            $fcm->type      = $type;
            $fcm->created      = date("Y-m-d H:i:s");
            if ($fcm->save() === false)
            {
                $errors   = array();
                $messages = $fcm->getMessages();
                foreach ($messages as $message)
                {
                    $e["message"] = $message->getMessage();
                    $e["field"]   = $message->getField();
                    $errors[]     = $e;
                }

                $this->log('error',' line '.__FILE__.'.'.__LINE__.' creating FCM ' . json_encode($errors));
            }
            else
            {
                $fcm_array[] = $fcm;
            }
        }
        
        $data[$key] = $token;

        $this->log('info',"FCM payload " . json_encode($data));

        //firebase server url to send the curl request
        $url = 'https://fcm.googleapis.com/fcm/send';

        $config = $this->di->getShared("config");

        //building headers for the request
        $headers = array(
            'Authorization: key=' . $config->firebase->key,
            'Content-Type: application/json'
        );

        //Initializing curl to open a connection
        $ch = curl_init();

        //Setting the curl url
        curl_setopt($ch,CURLOPT_URL,$url);

        //setting the method as post
        curl_setopt($ch,CURLOPT_POST,true);

        //adding headers 
        curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

        //disabling ssl support
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);

        //adding the fields in json format 
        curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode($data));

        //finally executing the curl request 
        $result = curl_exec($ch);
        if ($result === FALSE)
        {
            $this->log('error',curl_error($ch));
            return false;
        }

        //Now close the connection
        curl_close($ch);

        //and return the result 
        $this->log('info',$result);
        return $result;
    }

}
