<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Query\Builder as Builder;
use \Firebase\JWT\JWT;

class UserController extends ControllerBase
{

    public function indexAction()
    {
        
    }

	/**
	 * creates a new user
	 */
    public function create()
    {
        $request    = new Request();
        $json       = $request->getJsonRawBody();
		$this->log('info',__FILE__.".".__FUNCTION__.' REQUEST '.json_encode($json));

		$name  				 = isset($json->name) ? $json->name : false;
        $msisdn     		 = isset($json->msisdn) ? $json->msisdn : false;
        $device_model        = isset($json->device_model) ? $json->device_model : false;
		$device_os           = isset($json->device_os) ? $json->device_os : false;
		$device_version      = isset($json->device_version) ? $json->device_version : false;
		$device_manufacturer = isset($json->device_manufacturer) ? $json->device_manufacturer : false;
		$device_unique_id    = isset($json->device_unique_id) ? $json->device_unique_id : false;
		$version_code        = isset($json->version_code) ? $json->version_code : 3;
		$version_name        = isset($json->version_name) ? $json->version_name : 'Version 3';

		$msisdn = $this->formatMobileNumber($msisdn);

		if(!$msisdn){
			return $this->missingData('Invalid MSISDN');
		}
		
        if (!$device_model || !$device_os || !$device_version || !$device_manufacturer || !$device_unique_id || !$version_code || !$version_name )
        {
            return $this->missingData();
        }

		$device = [
			"model"=>$device_model,
			"os"=>$device_os,
			"version"=>$device_version,
			"manufacturer"=>$device_manufacturer,
			"unique_id"=>$device_unique_id
		];

		$application = ["version_number"=>$version_code,"version_name"=>$version_name];

		$data = ['msisdn' => $msisdn,'name' => $name,'device' => $device,"application"=>$application];

		$builder = User::query();

		$builder->where('msisdn', '=', $msisdn);

		if ($builder->count() > 0)
		{
			$user = $builder->first();

			if($user->device->unique_id != $device_unique_id){
				$security = $this->deviceKey(1024);
				$data['security'] = $security;
			}

			$user->update($data);
		}
		else {

			$security = $this->deviceKey(1024);
			$data['security'] = $security;
			User::create($data);
			$user = User::where('msisdn', $msisdn)->get();
		}

		$response = $user->toArray();
		$res = $response;

		$user = new stdClass();

		$msisdn = $response['msisdn'];
		$name = $response['name'];
		$public_key = isset($response['security']['public_key']) ? $response['security']['private_key'] : false;
		$id = $response['id'];

		$user->msisdn = $msisdn;
		$user->name = $name;
		$user->key = $public_key;
		$user->id = $id;

        return $this->systemResponse($user,200,'SUCCESS');
    }

	/**
	 * creates a new user
	 */
	public function get()
	{
		$request    = new Request();
		$json       = $request->getJsonRawBody();
		$this->log('info',__FILE__.".".__FUNCTION__.' REQUEST '.json_encode($json));

		$msisdns     		 = isset($json->msisdn) ? $json->msisdn : false;

		$msisdns = explode(':', $msisdns);

		$data = array();
		$users = array();

		foreach ($msisdns as $ms){

			$msisdn = $this->formatMobileNumber($ms);

			if($msisdn){

				$users[$msisdn] = ['msisdn'=>$ms,'id'=>'invalid'];
			}
			else {
				$users[$ms] = ['msisdn'=>$ms,'id'=>'invalid'];
			}

			$data[] = $msisdn;
		}

		$builder = User::query();
		$builder->inWhere('msisdn', $data);
		$response = $builder->get();

		$response = $response->toArray();

		foreach ($response as $res){

			$msisdn = $res['msisdn'];
			$name = $res['name'];
			$public_key = isset($res['security']['public_key']) ? $res['security']['public_key'] : false;
			$id = $res['id'];

			$user = ['msisdn'=>$msisdn,'name'=>$name,'key'=>$public_key,'id'=>$id];
			$users[$msisdn] = $user;
		}

		$data = array();

		foreach ($users as $key=>$usr){

			$data[] = $usr;
		}

		return $this->systemResponse($data,200,'SUCCESS');
	}

	/**
	 * checks if user exists
	 * @param $msisdn
	 *
	 * @return bool
	 */
    public function userExists($msisdn)
    {
		$builder = User::query();

		//allowed operators in where =, !=, >, <, >=, <=

		$builder->where('msisdn', '=', $msisdn);

        if ($builder->count() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
