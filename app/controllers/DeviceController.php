<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Query\Builder as Builder;
use \Firebase\JWT\JWT;

class DeviceController extends ControllerBase
{

    public function indexAction()
    {
        
    }

    public function create()
    {
        $request             = new Request();
        $json                = $request->getJsonRawBody();
        $this->log('info','JSON BODY '.json_encode($json));
        $device_model        = $json->device_model;
        $device_os           = $json->device_os;
        $device_token        = $json->device_token;
        $device_version      = $json->device_version;
        $device_manufacturer = $json->device_manufacturer;
        $device_unique_id    = $json->device_unique_id;
        $version_code        = isset($json->version_code) ? $json->version_code : 3;
        $version_name        = isset($json->version_name) ? $json->version_name : 'Version 3';
        $token               = $json->token;
        $referal_code        = isset($json->refered_by) ? $json->refered_by : false;
        $referer = false;



        if (!$device_os || !$device_manufacturer || !$device_model)
        {
            return $this->missingData();
        }
        $tokenData = $this->verifyToken($token,'openRequest');

        if (!$tokenData)
        {
            return $this->missingData();
        }

        $device = null;

        $was_refered = false;

        $new_device  = false;

        if ($device_unique_id)
        {
            $device      = Device::findFirst(" device_unique_id ='$device_unique_id' ");
        }

        if($referal_code) {
            $referal_code = $this->getSource($referal_code);
        }

        if (!$device)
        {
            $new_device = true;
            $device                   = new Device();
            $device->device_unique_id = $device_unique_id;
            $device->device_referal_code = $this->referalCode();
            if($referal_code)
            {
                $referal_code = $this->clean($referal_code);
                $referal_code = strtoupper($referal_code);
                $referer = Device::findFirst(" device_referal_code ='$referal_code' ");
                if($referer) {
                    $device->refered_by = $referer->device_id;
                    $device->device_balance = $this->config->bonus->referral->newUser + 30;
                }
            }
            else {
                $device->device_balance = 30;
            }

        }

        if(!$device->device_referal_code || is_null($device->device_referal_code) || strlen($device->device_referal_code) < 1)
        {
            $device->device_referal_code = $this->referalCode();
        }

        $key_pair                    = $this->deviceKey(1024);
        $device->device_os           = $device_os;
        $device->device_token        = $device_token;
        $device->device_version      = $device_version;
        $device->device_manufacturer = $device_manufacturer;
        $device->device_model        = $device_model;
        $device->created             = date("Y-m-d H:i:s");
        $device->device_key          = $key_pair['private'];
        $device->device_key_public   = $key_pair['public'];
        $device->code_version        = $version_code;
        $device->code_version_name   = $version_name;
        $device->referal_link      = $this->getReferalLink($device->device_referal_code);

        if ($device->save() === false)
        {
            $errors   = array();
            $messages = $device->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
            return $this->systemResponse($errors,421,"FAILED");
        }
        $device->device_key_public = '';
        $device->device_token      = $this->issueDeviceToken($device);

        if($referer)
        {
            $description =" first install, you will receive ".$this->config->bonus->referral->firstDownload." KES for his/her 1st download";
            $amount = $this->config->bonus->referral->referrer;
            $this->giveDiscount($device,$amount,$referer,$description);

            $amount = $this->config->bonus->referral->firstDownload;
            $description =" You have received $amount KES Referal Bonus";
            $this->giveDiscount($referer,$amount,$device,$description,false);
        }
        $device1 = $device;
        $device1->movie_price = 20;
        return $this->systemResponse($device1,200,'success');
    }

    /**
     * 
     * @param type $device_id
     * @return type Media
     */
    public function getMediaByID($device_id)
    {
        return Device::findFirst(array("device_id = '$device_id'"));
    }

    public function getMediaByMSISDN($msisdn)
    {
        return Device::findFirst(array("device_msisdn = '$msisdn'"));
    }

    public function all()
    {
        $request = new Request();
        $json    = $request->getJsonRawBody();
        $token   = $json->token;

        $tokenData = $this->verifyToken($token,'openRequest');

        if (!$tokenData)
        {
            return $this->missingData();
        }

        $sub_categories = Device::find();

        return $this->systemResponse($sub_categories);
    }

    public function update()
    {
        $request             = new Request();
        $json                = $request->getJsonRawBody();
        $device_os           = isset($json->device_os) ? $json->device_os : false;
        $device_model        = isset($json->device_model) ? $json->device_model : false;
        $device_manufacturer = isset($json->device_manufacturer) ? $json->device_manufacturer : false;
        $device_token        = isset($json->device_token) ? $json->device_token : false;
        $device_version      = isset($json->device_version) ? $json->device_version : false;
        $device_msisdn       = isset($json->device_msisdn) ? $json->device_msisdn : false;
        $device_id           = isset($json->device_id) ? $json->device_id : false;
        $token               = $json->token;

        $device_msisdn = $this->formatMobileNumber($device_msisdn);

        if (!$device_id)
        {
            $device = Device::findFirst("device_msisdn = $device_msisdn");
            if (!$device)
            {
                return $this->missingData();
            }
            else
            {
                $device_id                 = $device->device_id;
                $key_pair                  = $this->deviceKey(1024);
                $device->device_key        = $key_pair['private'];
                $device->device_key_public = $key_pair['public'];

                if ($device->save() === false)
                {
                    $errors   = array();
                    $messages = $device->getMessages();
                    foreach ($messages as $message)
                    {
                        $e["message"] = $message->getMessage();
                        $e["field"]   = $message->getField();
                        $errors[]     = $e;
                    }
                    return $this->systemResponse($errors,421,"FAILED");
                }
                unset($device->device_key_public);
            }
        }
        else
        {
            $device_id = $json->device_id;


            $device = Device::findFirst("device_id = $device_id");

            if (!$device)
            {
                return $this->missingData();
            }

            if (!$this->isValidDevice($token,$device))
            {
                return $this->accessDenied();
            }
        }

        if ($device_os)
        {
            $device->device_os = $device_os;
        }

        if ($device_token)
        {
            $device->device_token = $device_token;
        }

        if ($device_version)
        {
            $device->device_version = $device_version;
        }

        if ($device_manufacturer)
        {
            $device->device_manufacturer = $device_manufacturer;
        }

        if ($device_model)
        {
            $device->device_model = $device_model;
        }

        if ($device_msisdn)
        {
            $device->device_msisdn = $device_msisdn;
        }

        if ($device->save() === false)
        {
            $errors   = array();
            $messages = $device->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
            return $this->systemResponse($errors,421,"FAILED");
        }

        $device->device_token = $this->issueDeviceToken($device);
        $device->referal_link = $this->getReferalLink($device->device_referal_code);

        $response = new stdClass();
        $response->device_user = $device->device_user;
        $response->device_msisdn = $device->device_msisdn;
        $response->device_balance = $device->device_balance;
        $response->refered_by = $device->refered_by;
        $response->movies = intval($device->device_balance / 10);
        return $this->systemResponse($response);
    }

    public function delete()
    {

        $request   = new Request();
        $json      = $request->getJsonRawBody();
        $device_id = isset($json->device_id) ? $json->device_id : false;
        $token     = $json->token;

        if (!$device_id)
        {
            return $this->missingData();
        }
        $tokenData = $this->verifyToken($token,'openRequest');

        if (!$tokenData)
        {
            return $this->missingData();
        }

        $device = Device::findFirst(array("device_id = '$device_id'"));

        if (!$device)
        {
            return $this->notFound();
        }

        if ($device->delete() === false)
        {
            $errors   = array();
            $messages = $device->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
            return $this->systemResponse($errors,421,"FAILED");
        }

        return $this->systemResponse('Device Deleted');
    }

    public function table()
    {
        $this->view->disable();
        $request    = new Request();
        $token      = $request->getQuery('token');
        $sort       = $request->getQuery('sort');
        $per_page   = $request->getQuery('per_page');
        $page       = $request->getQuery('page');
        $filter_raw = $request->getQuery('filter');
        $start      = $request->getQuery('start');
        $end        = $request->getQuery('end');

        $filter_raw = trim($filter_raw);

        if ($filter_raw == 'undefined')
        {
            $filter_raw = false;
        }

        $filter = (isset($filter_raw) && strlen($filter_raw) > 3) ? $filter_raw : false;
        $start  = (isset($start) && $start != 'null') ? $start : false;
        $end    = (isset($end) && $end != 'null') ? $end : false;

        $extraWhere = array();

        $table = "device";

        $primaryKey = "device_id";

        if ($start && $end)
        {
            $extraWhere[] = "DATE($table.created) >= '$start' AND DATE($table.created) <= '$end' ";
        }

        if ($filter)
        {
            if (strlen($filter) > 3)
            {
                $extraWhere[] = "sub_category.device_os REGEXP '$filter' OR media.device_os REGEXP '$filter' ";
            }
        }

        if (count($extraWhere) > 0)
        {
            $where = implode(" AND ",$extraWhere);
        }
        else
        {
            $where = 1;
        }

        $joinQuery   = array();
        $joinQuery[] = " sub_category ON media.device_model = sub_category.device_model ";

        $fields [] = "$table.$primaryKey";
        $fields [] = "media.device_os";
        $fields [] = "sub_category.device_os as sub_category";
        $fields [] = "media.created";
        $fields [] = "media.device_manufacturer";
        $fields [] = "media.device_token";
        $fields [] = "media.device_key";
        $fields [] = "media.device_version";


        if (count($joinQuery) > 0)
        {
            $join = implode(" ",$joinQuery);
        }
        else
        {
            $join = '';
        }

        if (count($fields) > 0)
        {
            $fields = implode(",",$fields);
        }
        else
        {
            $fields = " $table.$primaryKey ";
        }

        if ($sort)
        {
            list($sortByColumn,$sortBy) = explode('|',$sort);
            $orderBy = "ORDER BY $sortByColumn $sortBy";
        }
        else
        {
            $orderBy = "";
        }
        $export = $request->getQuery('export');

        $export = isset($export) ? $export : 0;

        if ($export == 1)
        {
            $sql = "SELECT $fields "
                    . "FROM $table $join "
                    . "WHERE $where "
                    . "$orderBy ";

            return $this->exportQuery($sql);
        }

        $countQuery = "SELECT COUNT(`$table`.`$primaryKey`) id FROM `$table` $join WHERE 1";

        try
        {
            $total = $this->rawSelect($countQuery);
        }
        catch (Exception $e)
        {
            $this->logger->logMessage('error',__FUNCTION__ . "." . __LINE__,"Category: " . $e->getMessage(),0,$e->getCode());
            return $this->systemResponse("error occured",500,"Error Occured");
        }

        $total = $total[0]['id'];

        $last_page = $this->calculateTotalPages($total,$per_page);

        $current_page = $page - 1;

        if ($current_page)
        {

            $offset = $per_page * $current_page;
        }
        else
        {
            $current_page = 0;
            $offset       = 0;
        }

        if ($offset > $total)
        {

            $offset = $total - ($current_page * $per_page);
        }

        $from = $offset + 1;

        $current_page++;

        $left_records = $total - ($current_page * $per_page);

        $sql = "SELECT $fields "
                . "FROM $table $join "
                . "WHERE $where "
                . "$orderBy "
                . "LIMIT $offset,$per_page";

        $next_page_url = $left_records > 0 ? "device/table" : null;

        $prev_page_url = ($left_records + $per_page) < $total ? "device/table" : null;

        try
        {
            $transactions = $this->rawSelect($sql);
        }
        catch (Exception $e)
        {
            $this->logger->logMessage('error',__FUNCTION__ . "." . __LINE__,"Category: " . $e->getMessage(),0,$e->getCode());
            return $this->systemResponse("error occured",500,"Error Occured");
        }

        if ($transactions)
        {
            $tableData['total']         = $total;
            $tableData['per_page']      = $per_page;
            $tableData['next_page_url'] = $next_page_url;
            $tableData['prev_page_url'] = $prev_page_url;
            $tableData['current_page']  = $current_page;
            $tableData['last_page']     = $last_page;
            $tableData['from']          = $from;
            $tableData['to']            = $offset + count($transactions);

            $tableData['data'] = $transactions;

            return $this->systemResponse($tableData,200,"Success");
        }
        else
        {
            $tableData['data'] = [];
            return $this->systemResponse($tableData,200,"Not Found");
        }

        return $this->systemResponse($tableData,421,'Not Found');
    }

    /*
     * delete users
     */

    public function removeDevice()
    {

        $request   = new Request();
        $json      = $request->getJsonRawBody();
        $device_id = $json->device_id;
        $token     = $json->token;

        if (!$token)
        {
            return $this->missingToken();
        }

        $tokenData = $this->verifyToken($token,'openRequest');

        if (!$tokenData)
        {
            return $this->missingToken();
        }

        $subCategory = Device::findFirst(array("device_id=:id:",
                    'bind' => array("id" => $device_id)));
        if (!$subCategory)
        {
            return $this->notFound();
        }
        if ($subCategory->delete() === false)
        {
            $errors   = array();
            $messages = $subCategory->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
            return $this->systemResponse($errors,421,"FAILED");
        }

        return $this->systemResponse("Media Deleted");
    }

    /**
     * Give user a balance
     *
     * @param Device  $device
     * @param int     $amount
     * @param Device  $from
     * @param string $description
     * @param boolean $update
     */

    public function giveDiscount($device,$amount,$from,$description ,$update = true)
    {
        if($update) {
            $device->device_balance = $device->device_balance + $amount;
            $device->save();
        }
        $name = isset($from->device_user) ? $from->device_user : $from->device_msisdn;
        $message = "You have received KES ".number_format($amount)." bonus credit for referring ".$name." $description";

        $type      = 'all';
        $data                                 = array();
        $data['data']['balance']                 = $device->device_balance;
        $data['data']['message']                 = $message;
        $data['data']['title']                 = "Earned $amount KES Bonus";
        $data['data']['type']                 = "bonus";
        $data['notification']['body']         = "You have received $amount bonus";
        $data['notification']['title']        = "Congrats!! Earned $amount Bonus";
        $data['notification']['image']        = "ic_launcher";
        $data['notification']['click_action'] = "com.legitimate.imovie.view.Message";

        $fcm = new FirebaseMessageController();
        $fcm->sendPush($device,$type,$data);
    }

    /**
     * send low balance notification
     * @param Device $device
     */

    public function lowBalance($device)
    {
        $name = isset($device->device_user) ? $device->device_user : 'Hey!';
        $names = explode(' ',$name);
        $name = $names[0];
        $amount = $device->device_balance;

        $message = "$name,Your balance of ".number_format($amount)." KES is not enough to download a movie. Top Up you account";

        $type      = 'all';
        $data                                 = array();
        $data['data']['balance']                 = $device->device_balance;
        $data['data']['message']                 = $message;
        $data['data']['type']                 = "balance";
        $data['data']['title']                 = "Low Credits";
        $data['notification']['body']         = $message;
        $data['notification']['title']        = "Low Credits";
        $data['notification']['image']        = "ic_launcher";
        $data['notification']['click_action'] = "com.legitimate.imovie.view.Message";

        $fcm = new FirebaseMessageController();
        $fcm->sendPush($device,$type,$data);
    }

    public function balanceUpdate($device)
    {
        $type      = 'all';
        $data                                 = array();
        $data['data']['balance']                 = $device->device_balance;
        $fcm = new FirebaseMessageController();
        $fcm->sendPush($device,$type,$data);
    }

    public function getBalance()
    {
        $request    = new Request();
        $json       = $request->getJsonRawBody();
        $token      = $json->token;
        $device_id = $json->device_id;

        if (!$device_id)
        {
            return $this->missingData();
        }
        $device = Device::findFirst("device_id = $device_id");

        if (!$device)
        {
            return $this->missingData();
        }

        if (!$this->isValidDevice($token,$device))
        {
            return $this->accessDenied();
        }
        $response = new stdClass();
        $response->device_user = $device->device_user;
        $response->device_msisdn = $device->device_msisdn;
        $response->device_balance = $device->device_balance;
        $response->refered_by = $device->refered_by;
        $response->movies = intval($device->device_balance / 10);
        return $this->systemResponse($response);
    }

    public function getSource($string)
    {

        list($a,$b) = explode("=",$string,2);

        $part = utf8_decode(urldecode($b));

        $parts = explode("&",$part);

        $values = array();

        foreach($parts as $row)
        {
            list($k,$v) = explode("=",$row);
            $values[$k] = $v;
        }

        $src = isset($values['utm_source']) ? $values['utm_source'] : false;

        $this->log('infor','referal '.$string.' got this '.$src);

        return $src;
    }
}
