<?php

    use Firebase\JWT\JWT;
    use Phalcon\Logger\Adapter\File as FileAdapter;
    use Phalcon\Mvc\Controller;

    class ControllerBase extends Controller
{

    public function initialize()
    {

    }

	/**
	 * gets current datetime as string
	 * @return false|string
	 */

    public function getTime(){
    	return date("Y-m-d H:i:s");
	}

	public function getMongo(){

		$uri = "mongodb://".$this->config->mongodb->host.":".$this->config->mongodb->port ;
		$connection = new MongoClient($uri); // connect to a remote host at a given port
		$mongo = $connection->$this->config->mongodb->host;
	}

    /**
     * perfom curl post
     * @param type $url
     * @param type $postData
     * @return array of response and http staus code
     */
    public function curl($url,$postData)
    {
        $this->log('info',"posting to $url data " . json_encode($postData));
        $httpRequest = curl_init($url);
        curl_setopt($httpRequest,CURLOPT_NOBODY,true);
        curl_setopt($httpRequest,CURLOPT_POST,true);
        curl_setopt($httpRequest,CURLOPT_POSTFIELDS,json_encode($postData));
        curl_setopt($httpRequest,CURLOPT_TIMEOUT,30); //timeout after 30 seconds 
        curl_setopt($httpRequest,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($httpRequest,CURLOPT_HTTPHEADER,array('Content-Type: ' . 'application/json','Content-Length: ' . strlen(json_encode($postData))));
        curl_setopt($httpRequest,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($httpRequest,CURLOPT_HEADER,false);
        $resultData  = curl_exec($httpRequest);
        $status      = curl_getinfo($httpRequest,CURLINFO_HTTP_CODE);
        curl_close($httpRequest);

        $response = array("response" => $resultData,"status" => $status);

        $this->log('info',"received from $url " . json_encode($response));

        return $response;
    }

	/**
	 * @param        $message
	 * @param int    $statusCode
	 * @param string $statusDescription
	 */

    public function systemResponse($message,$statusCode = 200,$statusDescription = 'success')
    {
        $message = array('data' => $message,'status' => $statusCode);
        $this->log(1,json_encode($message));

        $this->response->setHeader("Content-Type","application/json");
        $this->response->setHeader("Access-Control-Allow-Origin","*");
        $this->response->setStatusCode($statusCode,$statusDescription);
        $this->response->setJsonContent($message);
        $this->response->send();
    }

    public function calculateTotalPages($total,$per_page)
    {
        if($per_page < 1){
        	return 0;
		}

    	$totalPages = (int) ($total / $per_page);

        if (($total % $per_page) > 0)
        {
            $totalPages = $totalPages + 1;
        }

        return $totalPages;
    }

    public function rawInsert($phql,$params = null)
    {
        try
        {
            $this->db->execute($phql,$params);
            $last_insert_id = $this->db->lastInsertId();
            return $last_insert_id;
        }
        catch (Exception $e)
        {
        	$this->log("error", "SQL ".$phql." PARAMS ".json_encode($params)." error ".$e->getMessage());
            throw $e;
        }
    }

	/**
	 * executes SQL Select statement
	 *
	 * @param      $statement
	 * @param null $params
	 *
	 * @return mixed
	 * @throws \Exception
	 */
    public function rawSelect($statement,$params = null)
    {
        try
        {
            $connection = $this->di->getShared("db");
            $success    = $connection->query($statement,$params);
            $success->setFetchMode(Phalcon\Db::FETCH_ASSOC);
            $success    = $success->fetchAll($success);
            return $success;
        }
        catch (Exception $e)
        {
			$this->log("error", "SQL ".$statement." PARAMS ".json_encode($params)." error ".$e->getMessage());
			throw $e;
        }
    }

	/**
	 * @return bool|\Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
	 */
    protected function isAuthenticated()
    {
        if ($this->session->has("user"))
        {
            return true;
        }
        return $this->response->redirect('login');
    }

    /**
     * Go Back from whence you came
     * @return type
     */
    protected function _redirectBack()
    {
        return $this->response->redirect($_SERVER['HTTP_REFERER']);
    }

    public function beforeExecuteRoute($dispatcher)
    {
        $self       = $_SERVER['REQUEST_URI'];
        $d          = explode('/',$self);
        $d          = array_reverse($d);
        $controller = $d[0];
        $action     = $d[1];

        if ($controller != 'login' && $controller != 'code' && $controller != 'submit')
        {
            $this->isAuthenticated();
            $user = $this->session->get('user');
            $this->view->setVars(['user' => $user]);

            $this->userAction("accessed $self","action");
        }
    }

    public function formatMobileNumber($mobile)
    {
        $mobile = preg_replace('/\s+/','',$mobile);
        $input  = substr($mobile,0,-strlen($mobile) + 1);
        $number = '';
        if ($input == '0')
        {
            $number = substr_replace($mobile,'254',0,1);
            return $number;
        }
        elseif ($input == '+')
        {
            $number = substr_replace($mobile,'',0,1);
        }
        elseif ($input == '7')
        {
            $number = substr_replace($mobile,'2547',0,1);
        }
        else
        {
            $number = $mobile;
        }
        return $number;
    }

    /**
     * Logs an application message
     * 
     * @param String $message Message to be logged
     * @param String $logLevel Logging level
     * 
     * @return void
     */
    public function log($logLevel = "info",$message="")
    {
        $infoLogger  = new FileAdapter(LOG_PATH . '/' . INFO_FILE);
        $errorLogger = new FileAdapter(LOG_PATH . '/' . ERROR_FILE);

        if (is_array($message) || is_object($message))
        {
            $message = json_encode($message);
        }

        $message = print_r($message,1);

        switch ($logLevel)
        {
            case 'error':
                $errorLogger->error($message);
                break;

            default:
                $infoLogger->info($message);
                break;
        }
    }

		public function send1($msisdn,$message)
		{
			$config = $this->di->getShared("config");

			$senderID = $config->sms->senderId;
			$username = $config->sms->username;
			$password = $config->sms->password;
			$url = $config->sms->url;

			$postData = array(
				"from" => $senderID,
				"to" => $msisdn,
				"text" => $message
			);

			$pass = base64_encode("$username:$password");

			$httpRequest = curl_init($url);
			curl_setopt($httpRequest, CURLOPT_NOBODY, true);
			curl_setopt($httpRequest, CURLOPT_POST, true);
			curl_setopt($httpRequest, CURLOPT_POSTFIELDS, json_encode($postData));
			curl_setopt($httpRequest, CURLOPT_TIMEOUT, 10);
			curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($httpRequest, CURLOPT_HTTPHEADER,
				array(
					'Content-Type: application/json',
					'Accept: application/json',
					'Authorization: Basic '.$pass
				)
			);
			$results = curl_exec($httpRequest);
			$httpStatusCode = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
			curl_close($httpRequest);

			$response = array("response" => $results,"status" => $httpStatusCode);
			$this->log('info',"received from $url status $httpStatusCode results $results");

			return json_decode($response);

		}

    public function sendMessage($msisdn,$message)
    {
		$config = $this->di->getShared("config");

		$senderID = $config->sms->senderId;
		$username = $config->sms->username;
		$password = $config->sms->password;
		$url = $config->sms->url;

		$postData = array(
			"from" => $senderID,
			"to" => $msisdn,
			"text" => $message
		);

		$pass = base64_encode("$username:$password");

		$httpRequest = curl_init($url);
		curl_setopt($httpRequest, CURLOPT_NOBODY, true);
		curl_setopt($httpRequest, CURLOPT_POST, true);
		curl_setopt($httpRequest, CURLOPT_POSTFIELDS, json_encode($postData));
		curl_setopt($httpRequest, CURLOPT_TIMEOUT, 10);
		curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($httpRequest, CURLOPT_HTTPHEADER,
			array(
				'Content-Type: application/json',
				'Accept: application/json',
				'Authorization: Basic '.$pass
			)
		);
		$results = curl_exec($httpRequest);
		$httpStatusCode = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
		curl_close($httpRequest);

		$response = array("response" => $results,"status" => $httpStatusCode);
		$this->log('info',"received from $url status $httpStatusCode results $results message $message");

		return $response;
    }

    public function missingToken()
    {
        $data = array('data' => "you do not have permission to access the requested resource",'status' => 403);
        $this->log('error',json_encode($data));
        return $this->systemResponse($data,403,"Access Denied");
    }

    public function invalidToken()
		{
			$data = array('data' => "you do not have permission to access the requested resource, invalid token supplied",'status'	=> 403);
			$this->log('error',json_encode($data));
			return $this->systemResponse($data,403,"Access Denied");
		}

	public function accessDenied()
    {
        $this->log('error','you do not have permission to access the requested resource');

        return $this->systemResponse("you do not have permission to access the requested resource",403,"Access Denied");
    }

    public function lowBalanceResponse()
    {
        return $this->systemResponse("you do not have enough credits to access the requested resource",403,"Access Denied");
    }

    public function missingData($data = "required fields are missing")
    {
        $this->log('error',$data);

        return $this->systemResponse($data,421,'MISSING FIELDS');
    }

    public function notFound($data = "resource not found")
    {
        $this->log('error',$data);
        return $this->systemResponse($data,404,'NOT FOUND');
    }

    public function randomCode($length = 5,$type = 'integer')
    {
    	//return 1234;

        $numeric = "23456789";

        $characters = "ABCDEFGHJKMNPQRSTUVWXYZ";

        if ($type == 'integer')
        {
            $characters = $numeric;
        }
        else if ($type == 'alphanumeric')
        {
            $characters .= $numeric;
        }

        $result = " ";

        for ($i = 0; $i < $length; $i++)
        {
            $result .= $characters[mt_rand(0,strlen($characters) - 1)];
        }
        return trim(strtoupper($result));
    }

    public function sendSMS($msisdn,$type,$params)
    {
        switch ($type)
        {
            case 'user_reset_password':
            case 'user_register':
                $code    = isset($params['code']) ? $params['code'] : false;
                $name    = isset($params['name']) ? $params['name'] : 'User';
                $message = "Dear $name your new password is $code ";
                $this->sendMessage($msisdn,$message);
                break;
        }
    }

    /**
     * @param String $token
     * @param Device $device
     * @return int
     */

    public function isValidDevice($token,$device)
    {
        JWT::$leeway = 20;

        $key = $device->device_unique_id;
        try
        {
            $decoded = JWT::decode($token,$key,array('HS256'));
        }
        catch (Exception $e)
        {
            $this->log('error',$e->getMessage());
            return 0;
        }
        if (isset($decoded->device_id) && $decoded->device_id == $device->device_id)
        {
            return 1;
        }

        return 0;
    }

	/**
	 * @param $token
	 * @param User $user
	 *
	 * @return int
	 */

    public function isValidUser($token,$user)
		{
			if(!is_null($user->deleted) || strlen($user->deleted) > 0 || $user->status == 0){
				return false;
			}

			JWT::$leeway = 20;

			$key = $user->password;

			try
			{
				$decoded = JWT::decode($token,$key,array('HS256'));
			}
			catch (Exception $e)
			{
				$this->log('error',$e->getMessage());
				return 0;
			}
			if (isset($decoded->id) && $decoded->id == $user->id)
			{
				return 1;
			}

			return 0;
		}

	/**
	 * @param \User $user
	 *
	 * @return string
	 */

    public function issueUserToken($user)
    {
        $key        = $user->password;
        $issuedAt   = time();
        $notBefore  = $issuedAt;             //Adding 10 seconds
        $expire     = $notBefore + 60;            // Adding 60 seconds
        $serverName = gethostname(); // Retrieve the server name from config file

        $token = array(
            "iss"       => $serverName,
            "iat"       => $issuedAt,
            "nbf"       => $notBefore,
            "id" 		=> $user->id
        );

        $jwt   = JWT::encode($token,$key);
        return $jwt;
    }

    public function deviceKey($length = 1024)
    {
        if (!isset($length) || intval($length) <= 512)
        {
            $length = 512;
        }

        $length = intval($length);

        $config = array(
            "digest_alg"       => "sha512",
            "private_key_bits" => $length,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        );

        // Create the private and public key
        $res = openssl_pkey_new($config);

        // Extract the private key from $res to $privKey
        openssl_pkey_export($res,$privKey);

        // Extract the public key from $res to $pubKey
        $pubKey = openssl_pkey_get_details($res);

        $pubKey = $pubKey["key"];

        return array('public_key' => $pubKey,'private_key' => $privKey);
    }

    public function downloadCode($length = 8)
    {
        if (!isset($length))
        {
            $length = 8;
        }

        do
        {
            if (function_exists('random_bytes'))
            {
                $code = bin2hex(random_bytes($length));
            }
            if (function_exists('mcrypt_create_iv'))
            {
                $code = bin2hex(mcrypt_create_iv($length,MCRYPT_DEV_URANDOM));
            }
            if (function_exists('openssl_random_pseudo_bytes'))
            {
                $code = bin2hex(openssl_random_pseudo_bytes($length));
            }
        }
        while (Download::findFirst("code = '$code'"));

        return $code;
    }

    public function referalCode($length = 5)
    {
        if (!isset($length))
        {
            $length = 5;
        }

        do
        {
            $code = $this->randomCode($length,'alphanumeric');

            $code = strtoupper($code);
        }
        while (Device::findFirst("device_referal_code = '$code'"));

        return $code;
    }

    public function verifyToken($token,$action = 0)
    {
        $key = "IMw2c3W5KWLFN1sBH1befeFocdWUs0Sd";

        $decoded = '';

        try
        {
            $decoded = JWT::decode($token,$key,array('HS256'));
        }
        catch (Exception $e)
        {
            $this->log('error',$e->getMessage());
            return $decoded;
        }
        if ($decoded->action == $action)
        {
            return $decoded;
        }
        if (isset($decoded->name) && isset($decoded->userId))
        {
            return $decoded;
        }

        return;
    }

    public function verifyUser($key,$token)
		{
			$decoded = '';

			try
			{
				$decoded = JWT::decode($token,$key,array('HS256'));
			}
			catch (Exception $e)
			{
				$this->log('error',$e->getMessage());
				return $decoded;
			}
			if ($decoded->action == $action)
			{
				return $decoded;
			}
			if (isset($decoded->name) && isset($decoded->userId))
			{
				return $decoded;
			}

			return;
		}

		/**
		 * @param \User $user
		 *
		 * @return string
		 */
    public function issueToken($user)
    {
        $key = $user->password;
        $issuedAt   = time();
        $notBefore  = $issuedAt + 1;             //Adding 10 seconds
        $expire     = $notBefore + 60;            // Adding 60 seconds
        $serverName = gethostname(); // Retrieve the server name from config file

        $token = array(
            "iss"     => $serverName,
            "iat"     => $issuedAt,
            "nbf"     => $notBefore,
            "id" => $user->id,
			"client_id" => 1
        );
        $jwt   = JWT::encode($token,$key);
        return $jwt;
    }

    /**
     * gets the ordinal suffix for the given date, corrects invalid date
     * @param type $day
     * @param type $daysInMonth
     * @return type
     */
    protected function formatDayOfMonth($day,$daysInMonth = null)
    {
        if (!is_null($daysInMonth))
        {
            if ($day > $daysInMonth)
            {
                $day = $day - $daysInMonth;
            }
        }
        if ($day == 1 || $day == 21 || $day == 31)
        {
            return $day . 'st';
        }
        else if ($day == 2 || $day == 22)
        {
            return $day . 'nd';
        }
        else if ($day == 3 || $day == 23)
        {
            return $day . 'rd';
        }
        else
        {
            return $day . "th";
        }
    }

    /**
     *  gets month name from int
     * @param int $month
     * @return string
     */
    protected function getMontName($month)
    {
        if ($month > 0)
        {
            $month--;
        }
        if ($month > 12)
        {
            $month = 11;
        }
        $names = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOW","DEC"];
        return $names[$month];
    }

    /**
     * loops through graph series, where there is data or is empty fill with zero values to avoid gaps in graph
     *  
     * @param type $type graph type i.e daily, hourly, monthly or yearly
     * @param type $labels  graph object (i.e value and label )
     * @param type $count series to generate
     * @param type $start
     * @return array
     */
    protected function fillEmptySpaces($type,$labels,$count,$start)
    {
        $count++;
        switch ($type)
        {
            case 'hourly':
                $n               = 1;
                //$count = 24;
                $current         = 0; //$start->hour;
                $formated_labels = array();
                while ($n <= $count)
                {
                    if ($current > 23)
                    {
                        $current = 0;
                    }
                    if (!isset($labels['day-' . $current]))
                    {
                        $object                    = new stdClass();
                        $object->label             = $current . ' Hrs';
                        $object->value             = 0;
                        $labels['day-' . $current] = $object;
                    }
                    else
                    {
                        $labels['day-' . $current]->label = $labels['day-' . $current]->label . ' Hrs';
                    }
                    array_push($formated_labels,$labels['day-' . $current]);
                    $n++;
                    $current++;
                }
                return $formated_labels;
                break;

            case 'daily':
                $n               = 1;
                $current         = $start->day;
                $formated_labels = array();
                while ($n <= $count)
                {
                    if (!isset($labels['day-' . $current]))
                    {
                        $object                    = new stdClass();
                        $object->label             = $this->formatDayOfMonth($current,$start->daysInMonth);
                        $object->value             = 0;
                        $labels['day-' . $current] = $object;
                    }
                    else
                    {
                        $labels['day-' . $current]->label = $this->formatDayOfMonth($labels['day-' . $current]->label);
                    }
                    array_push($formated_labels,$labels['day-' . $current]);
                    $n++;
                    $current++;

                    if ($current > $start->daysInMonth)
                    {
                        $current = 1;
                    }
                }
                return $formated_labels;
                break;

            case 'monthly':
                $formated_labels = array();
                $current         = $start->month;
                $n               = 1;
                while ($n <= $count)
                {
                    if (!isset($labels['day-' . $current]))
                    {
                        $object                    = new stdClass();
                        $object->label             = $this->getMontName($current);
                        $object->value             = 0;
                        $labels['day-' . $current] = $object;
                    }
                    else
                    {
                        $labels['day-' . $current]->label = $this->getMontName($labels['day-' . $current]->label);
                    }
                    array_push($formated_labels,$labels['day-' . $current]);
                    $n++;
                    $current++;
                }
                return $formated_labels;
                break;

            case 'yearly':
                $formated_labels = array();
                $current         = $start->year;
                $n               = 1;
                while ($n <= $count)
                {
                    if (!isset($labels['day-' . $current]))
                    {
                        $object                    = new stdClass();
                        $object->label             = $current;
                        $object->value             = 0;
                        $labels['day-' . $current] = $object;
                    }

                    array_push($formated_labels,$labels['day-' . $current]);
                    $n++;
                    $current++;
                }
                return $formated_labels;
                break;
        }
    }
    
    public function getReferalLink($code){

        $str = "referrer=utm_source%3D$code%26utm_medium%3Dreferral%26utm_term%3Dfree%252Bmovie%26utm_content%3D$code%26utm_campaign%3Dinstalls";
        //return "https://play.google.com/store/apps/details?id=com.legitimate.imovieKE&referrer=utm_source%3D$code";
        //return "https://play.google.com/store/apps/details?id=com.legitimate.imovieKE&referrer=$code";
        return "https://play.google.com/store/apps/details?id=com.legitimate.imovieKE&$str";
        //referrer=utm_source%3D$code%26utm_term%3Dfree%252Bmovie";

    }

    public function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

	/**
	 * @param $msisdn
	 *
	 * @return bool|\Phalcon\Mvc\Model\ResultInterface|\Profile
	 */

	public function profileExists($msisdn)
	{
		$profile = Profile::findFirst(array("msisdn=:username:",'bind' => array("username" => $msisdn)));

		if($profile){

			return $profile;

		}

		return false;
	}

		/**
		 * @param $msisdn
		 *
		 * @return \Phalcon\Mvc\Model\ResultInterface|\Profile|void
		 */

	public function createProfile($msisdn)
		{
			$profile = Profile::findFirst(array("msisdn=:username:",'bind' => array("username" => $msisdn)));

			if($profile){

				return $profile;

			}

			$profile = new Profile();
			$profile->msisdn = $msisdn;
			$profile->created = $this->getTime();
			$profile->status  = 1;

			if ($profile->save() === false)
			{
				$errors   = array();
				$messages = $profile->getMessages();
				foreach ($messages as $message)
				{
					$e["message"] = $message->getMessage();
					$e["field"]   = $message->getField();
					$errors[]     = $e;
				}
				return $this->systemResponse($errors,421,"profile creation failed");
			}

			$profileSetting = new ProfileSetting();
			$profileSetting->profile_id 	= $profile->id;
			$profileSetting->client_id	 	= 1;
			$profileSetting->status = 1;
			$profileSetting->created   = $this->getTime();

			if ($profileSetting->save() === false)
			{
				$errors   = array();
				$messages = $profileSetting->getMessages();
				foreach ($messages as $message)
				{
					$e["message"] = $message->getMessage();
					$e["field"]   = $message->getField();
					$errors[]     = $e;
				}

				$this->log("error","HII" . json_encode($errors));

				return $this->systemResponse($errors,421,"Could not map profile to client for the newly created user");
			}

			return $profile;
		}


	}
